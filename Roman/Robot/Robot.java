package ru.Roman.Robot;

import ru.Roman.Robot.Direction;

/**
 * Класс передвижения робота по координатам.
 *
 * @author Р. Петрушов.
 * @version 1.0
 * @since 16.06.2016
 */

public class Robot {

    /**
     * x - начальная координата робота на оси абсцисс.
     * y - начальная координата робота на оси ординат.
     * direction - начальное направление робота (UP, DOWN, LEFT, RIGHT).
     */
    private int x;
    private int y;
    private Direction direction;
    
    /**
     * Начальные координаты и направление по умолчанию.
     * x = 0;
     * y = 0;
     * direction.UP
     */

    public Robot() {
        this(0, 0, Direction.UP);
    }

    public Robot(int x, int y, Direction direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Direction getDirection() {
        return direction;
    }

    @Override
    public String toString() {
        return "Robot{" +
                "x=" + x +
                ", y=" + y +
                ", direction=" + direction +
                '}';
    }

    /**
     * повернуться на 90 градусов против часовой стрелки
     */
    public void turnLeft() {
        switch (direction) {
            case UP:
                direction = Direction.LEFT;
                break;
            case LEFT:
                direction = Direction.DOWN;
                break;
            case DOWN:
                direction = Direction.RIGHT;
                break;
            case RIGHT:
                direction = Direction.UP;
                break;
        }

    }

    /**
     * повернуться на 90 градусов по часовой стрелке
     */
    public void turnRight() {
        switch (direction) {
            case UP:
                direction = Direction.RIGHT;
                break;
            case RIGHT:
                direction = Direction.DOWN;
                break;
            case DOWN:
                direction = Direction.LEFT;
                break;
            case LEFT:
                direction = Direction.UP;
                break;
        }

    }

    /**
     * шаг в направлении взгляда
     * за один шаг робот изменяет одну свою координату на единицу
     */
    public void stepForward() {
        switch (direction) {
            case UP:
                y++;
                break;
            case DOWN:
                y--;
                break;
            case RIGHT:
                x++;
                break;
            case LEFT:
                x--;
                break;
        }
    }
}
