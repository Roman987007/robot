package ru.Roman.Robot;

/**
 * Класс передвижения робота от начальных до конечных координатах.
 * @author Р. Петрушов
 * @since 16.06.2016
 * @version 1.0
 */
public class Main {
    public static void main(String[] args) {


        Robot robot = new Robot();
        //Вывод начальных координат робота, на экран.
        System.out.println(robot);

        moveRobot(10, 10, robot);
    }

        /**
         * Метод алгоритма передвижения робота из начальных координат в заданном направлении в конечные координаты.
         *
         * @param endX  конечная координата робота на оси абсцисс.
         * @param endY  конечная координата робота на оси ординат.
         * @param robot передвижение робота.
         */
    private static void moveRobot(int endX, int endY, Robot robot) {
        if (robot.getX() < endX) {
            while (robot.getDirection() != Direction.RIGHT) {
                robot.turnLeft();

            }
            System.out.println(robot.toString());
        } else {
            while (robot.getDirection() != Direction.LEFT) {
                robot.turnRight();

            }
            System.out.println(robot.toString());

        }


        while (robot.getX() != endX) {
            robot.stepForward();
            System.out.println(robot.toString());
        }


        if (robot.getY() < endY) {
            while (robot.getDirection() != Direction.UP) {
                robot.turnRight();
                System.out.println(robot.toString());
            }
        } else {
            while (robot.getDirection() != Direction.DOWN) {
                robot.turnLeft();
                System.out.println(robot.toString());
            }
        }
        while (robot.getY() != endY) {
            robot.stepForward();
            System.out.println(robot.toString());
        }
        System.out.println(robot.toString());
    }
}


