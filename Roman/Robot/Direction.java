package ru.Roman.Robot;

/*
*Направление робота.
*/

public enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
}
